<?php

$SPONSORS = array(
    array (
        "imgSrc"    => "geminiso.png",
        "link"      => "https://www.gemiso.com/"
    )
    ,
    array (
        "imgSrc"    => "kwu.png",
        "link"      => "https://www.kw.ac.kr/en/"
    )
    ,
    array (
        "imgSrc"    => "mux.png",
        "link"      => "https://mux.com/"
    )
    ,
    array (
        "imgSrc"    => "youtube_logo.svg",
        "link"      => "https://youtube.com/"
    )
    ,
    array (
        "imgSrc"    => "videolabs.png",
        "link"      => "https://videolabs.io/"
    )
);

function getSponsors() {
    global $SPONSORS;
    return $SPONSORS;
}
?>
