<?php
   $title = "12th Video Dev Days, September 23-24, 2023";
   $body_color = "orange";

   $new_design = true;
   $show_vdd_banner = false;
   $lang = "en";
   $menu = array( "videolan", "events" );

   $rev = time();

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/videolan/events/vdd23/style.css?$rev");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

  <div class="sponsor-box-2 d-xs-none d-md-block">
    <h4>Sponsors</h4>
    <a href="https://videolabs.io/" target="_blank">
        <?php image( 'events/vdd19/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo'); ?>
    </a>
    <a href="https://mux.com/" target="_blank">
        <?php image( 'events/vdd19/sponsors/mux.png' , 'Mux', 'sponsors-logo'); ?>
    </a>
  </div>
  <header class="header-bg">
      <div class="container">
        <div class="row col-lg-10 col-lg-offset-1">
          <div class="col-sm-12 col-md-4">
            <img class="img-fluid center-block" style="max-width:100%; height:auto; border-radius: 4%;" src="/images/events/vdd23/vdd23.png" alt="vdd logo">
          </div>
          <div class="col-sm-12 col-md-8 text-center align-middle">
            <h1 id="videodevdays">Video Dev Days 2023</h1>
            <h3>The Open Multimedia Conference that frees the cone in you!</h3>
            <h4>23 - 24 September, 2023, Dublin</h4>
          </div>
        </div>
      </div>
   </header>

<section id="overview">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">
			 About
          <span class="spacer-inline"></span>
        </h2>

			<p><a href="/videolan/">VideoLAN non-profit organization</a> is pleased to welcome you to the next multimedia open-source event in Dublin!</p>
			<p>In the upcoming edition, individuals from the VideoLAN and open-source multimedia communities will converge in <strong>Dublin</strong> to discuss and collaborate on shaping the future of the open-source multimedia.</p>
			<p>This technical conference will concentrate on low-level multimedia elements, encompassing codecs and their implementations like <a href="https://www.videolan.org/developers/x264.html">x264</a> or <a href="https://code.videolan.org/videolan/dav1d">dav1d</a>, frameworks like FFmpeg or LibVLC, and lots of other exciting projects in the multimedia world.</p>

        <p>This is a <strong>technical</strong> conference, focused on low-level multimedia, like codecs and their implementations like <a href="https://www.videolan.org/developers/x264.html">x264</a> or <a href="https://code.videolan.org/videolan/dav1d">dav1d</a>, frameworks like FFmpeg or Gstreamer or playback libraries like libVLC.</p>
        
		<div>
		  <div class="row">
          <div class="col-md-6">
            <div class="text-box" id="when-box">
              <h4 class="text-box-title">When ?</h4>
              <p class="text-box-content">23 - 24 September 2023</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box" id="where-box">
              <h4 class="text-box-title">Where ?</h4>
              <p class="text-box-content">Dublin, Ireland</p>
            </div>
          </div>
        </div>

		  <div class="row">
          <div class="col-md-6">
            <div class="text-box" id="who-come-box">
              <h4 class="text-box-title">For ?</h4>
              <p class="text-box-content">
        				<strong>Anyone</strong> who cares about open source multimedia technologies and development.
				  </p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box" id="partners-box">
              <h4 class="text-box-title">From ?</h4>
              <p class="text-box-content">VideoLAN. But you are very welcome to co-sponsor the event.</p>
            </div>
          </div>
        </div>
 		
		</div>

      </div>
    </div>
  </div>
</section>

<section id="schedule">
  <div class="container">
    <div class="row">
      <div class="text-center">
        <h2 class="uppercase">Schedule</h2>
        <hr class="spacer-2">
      </div>
      <div class="col-lg-10 col-lg-offset-1">
      <!-- Nav tabs -->

      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#friday" aria-controls="friday" role="tab" data-toggle="tab">Friday 22</a></li>
        <li role="presentation" class="active"><a href="#saturday" aria-controls="saturday" role="tab" data-toggle="tab">Saturday 23</a></li>
        <li role="presentation"><a href="#sunday" aria-controls="sunday" role="tab" data-toggle="tab">Sunday 24</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade" id="friday">
            <div class="event">
              <h4 class="event-time">09:00</h4>
              <div class="event-description">
                <h3>Community Bonding Day</h3>
                <p>
                Join a group of new friends for a treasure hunt inside the City of Dublin.<br>
                <strong>Departing 09:00</strong> from 
                Clayton Hotel Cardiff Lane, Sir John Rogerson's Quay, Dublin Docklands, Dublin, D02 YT21
                </p>
              </div>
            </div>
            <div class="event">
              <h4 class="event-time">19:00</h4>
              <div class="event-description">
                <h3>Evening drinks</h3>
                <p>On <strong>Friday from 19h00</strong> people are welcome to come and share a few good drinks, with all attendees.</p>
                <p>Alcoholic and non-alcoholic drinks will be available !</p>
                <p>Location: BrewDog OutPost Dublin, Three Locks Square, 4, Dublin Docklands, Dublin 2, D02 E5R7</p>
              </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade active in" id="saturday">
            <div class="event event-breakfast">
              <h4 class="event-time">
                09:00
              </h4>
              <div class="event-description">
                <h3>Registration</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:30 - 09:50
              </h4>
              <p class="event-author">
                <span class="avatar avatar-jb"></span>Jean-Baptiste Kempf, VideoLAN
              </p>
              <div class="event-description">
                <h3>Opening remarks</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:50 - 10:20
              </h4>
              <p class="event-author">
                Anton Khirnov
              </p>
              <div class="event-description">
                <h3>Multithreading in FFmpeg</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                10:20 - 10:50
              </h4>
              <p class="event-author">
                Christophe Gisquet
              </p>
              <div class="event-description">
                <h3>Tools for Screen Sharing in AV1</h3>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                10:50 - 11:10
              </h4>
              <div class="event-description">
                <h3>Break</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:10 - 11:40
              </h4>
              <p class="event-author">
                Rémi Denis-Courmont
              </p>
              <div class="event-description">
                <h3>RV and RVV Introduction</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:40 - 12:10
              </h4>
              <p class="event-author">
                Nicolas Pomepuy
              </p>
              <div class="event-description">
                <h3>VLC Android Technical Stack</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                12:10 - 12:40
              </h4>
              <p class="event-author">
                Ronald S Bultje
              </p>
              <div class="event-description">
                <h3>dav1d in 2023</h3>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                12:40 - 14:00
              </h4>
              <div class="event-description">
                <h3>Lunch break</h3>
              </div>
            </div>

            <div class="event event-meetups">
              <h4 class="event-time">
                14:00 - 16:00
              </h4>
              <div class="event-description">
                <h3>Meetups session 1/2</h3>
                <div class="row">
                	<div class="col-md-4"><strong>Room 1</strong></div>
                	<div class="col-md-4"><strong>Room 2</strong></div>
                	<div class="col-md-4"><strong>Room 3</strong></div>
                </div>
                <div class="row">
                	<div class="col-md-4">VLC Media Player</div>
                	<div class="col-md-4">AV1</div>
                	<div class="col-md-4">Others</div>
                </div>
              </div>
            </div>

            <div class="event event-meetups">
              <h4 class="event-time">
                16:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Meetups session 2/2</h3>
                <div class="row">
                	<div class="col-md-4"><strong>Room 1</strong></div>
                	<div class="col-md-4"><strong>Room 2</strong></div>
                	<div class="col-md-4"><strong>Room 3</strong></div>
                </div>
                <div class="row">
                	<div class="col-md-4"><p>VLC Mobile</p></div>
                	<div class="col-md-4"><p>FFmpeg</p></div>
                	<div class="col-md-4"><p>Others</p></div>
                </div>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                19:00 - 21:00
              </h4>
              <div class="event-description">
                <h3>Community Dinner</h3>
                <p>You should have received a pre-order form by email</p>
                <p>Location: Merchants Arch, 48-49 Wellington Quay, Temple Bar, Dublin, D02 EY65</p>
              </div>
            </div>

        </div>

        <div role="tabpanel" class="tab-pane fade" id="sunday">

            <div class="event event-talk">
              <h4 class="event-time">
                09:30 - 09:50
              </h4>
              <p class="event-author">
                Martin Storsjö
              </p>
              <div class="event-description">
                <h3>llvm-mingw</h3>
              </div>
            </div>
            
            <div class="event event-talk">
              <h4 class="event-time">
                09:30 - 09:50
              </h4>
              <p class="event-author">
                Mark Shwartzman
              </p>
              <div class="event-description">
                <h3>FFmpeg for Live at Meta	</h3>
              </div>
            </div>
            
            <div class="event event-talk">
              <h4 class="event-time">
                09:50 - 10:10
              </h4>
              <p class="event-author">
                Rémi Denis-Courmont
              </p>
              <div class="event-description">
                <h3>The pointer provenance problem in C & co.</h3>
              </div>
            </div>
            
            <div class="event event-talk">
              <h4 class="event-time">
                10:10 - 10:30
              </h4>
              <p class="event-author">
                Christophe Gisquet
              </p>
              <div class="event-description">
                <h3>Reading bits faster in libavcodec	</h3>
              </div>
            </div>
            
            <div class="event event-talk">
              <h4 class="event-time">
                10:30 - 10:50
              </h4>
              <p class="event-author">
                Fredrik Lundkvist
              </p>
              <div class="event-description">
                <h3>SVT Open-content</h3>
              </div>
            </div>
            
            <div class="event event-talk">
              <h4 class="event-time">
                10:50 - 11:10
              </h4>
              <p class="event-author">
                Paul Mahol
              </p>
              <div class="event-description">
                <h3>Remote Presentation about Reverse Engineering of popular and/or dead codecs</h3>
              </div>
            </div>
            
            <div class="event event-talk">
              <h4 class="event-time">
                11:10 - 12:00
              </h4>
              <div class="event-description">
                <h3>Lightning talks</h3>
              </div>
            </div>


        </div>
        
      </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Sponsors</h2>
    <hr class="spacer-2">

    <section class="text-center">
     <div class="row col-xs-10 col-xs-offset-1">
	     <div class="col col-md-6 col-sm-12">
        	<a href="https://videolabs.io/" target="_blank">
	          <?php image( 'events/vdd19/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo-2'); ?>
	        </a>
	      </div>
        <div class="col col-md-6 col-sm-12">
         <a href="https://mux.com/" target="_blank">
          <?php image( 'events/vdd19/sponsors/mux.png' , 'Mux', 'sponsors-logo-2'); ?>
         </a>
        </div>
     </div>
    </section>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Access</h2>
    <hr class="spacer-2">
    <div class="row">
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Conference</h3>
			<p>Trinity College, College Green, Dublin 2</p>
			<p>Synge Theatre, Rooms 3051, 3071 and 3074</p>
			<iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=-6.260790824890138%2C53.341078409602595%2C-6.250813007354736%2C53.34638196066375&amp;layer=mapnik" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=17/53.34373/-6.25580">Open map in full</a></small>
      </div>
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Hotel</h3>
			<p>If you have been sponsored, your hotel will be: Clayton Hotel Cardiff Lane, Sir John Rogerson's Quay, Dublin Docklands, Dublin, D02 YT21</p>
			<iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=-6.246344447135925%2C53.343762293854404%2C-6.23926341533661%2C53.346535676943994&amp;layer=mapnik" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=17/53.34373/-6.25580">Open map in full</a></small>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Beer Event</h3>
			<p>BrewDog OutPost Dublin, Three Locks Square, 4, Dublin Docklands, Dublin 2, D02 E5R7</p>
			<iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=-6.232592761516572%2C53.34391281696589%2C-6.229052245616914%2C53.34529952616522&amp;layer=mapnik" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/53.34461/-6.23082">Afficher une carte plus grande</a></small>      </div>
      <div class="col-md-6">
     </div>
    </div>
  </div>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="uppercase">Code of Conduct </h2>
        <p>This community activity is running under the <a href="https://wiki.videolan.org/CoC/">VideoLAN Code of Conduct</a>. We expect all attendees to respect our <a href="https://wiki.videolan.org/VideoLAN_Values/">Shared Values</a>.</p>
      </div>
      <div class="col-md-6">
        <h2 class="uppercase">Contact </h2>
        <p>The VideoLAN Dev Days are organized by the board members of the VideoLAN non-profit organization, Jean-Baptiste Kempf, Denis Charmet, Felix Paul Khüne and Konstantin Pavlov. You can reach us at <span style="color: #39b549">board@videolan.org</span>.</p>
      </div>
    </div>
  </div>
</section>

<?php footer('$Id: index.php 5400 2009-07-19 15:37:21Z jb $'); ?>
