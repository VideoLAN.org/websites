<center><h1 class='bigtitle' style="padding-bottom: 3px;">VLC <b>3.0.12</b> and <b>3.0.13</b> <em>automatic updater issue</em></h1>
<div style="padding-top: 0px; padding-bottom: 10px; color: grey;">A bug in the auto updater will prevent Windows users to automatically update</div>
</center>

<center><h2>This is only relevant to Windows users</h2></center>

<h3>Short version:</h3>
<ul>
<li>- versions 3.0.12 to 3.0.13 are <b>not</b> able to update automatically anymore, and will <b>require</b> user action<li>
<li>- versions 3.0.11 and earlier should update automatically to 3.0.14</li>
</ul>
<br/>

<h3>Description:</h3>
This notice applies to VLC 3.0.13 and VLC 3.0.12 users.<br/>
Due to a mistake introduced in the automatic updater code, updates will be downloaded, verified for integrity, but will not be installed. This is bad and we would like to apologize for this.<br/><br/>

<h3>Instructions:</h3>
In order to update to 3.0.14, you will need to go to <a href="https://www.videolan.org/vlc">https://www.videolan.org/vlc</a> to download and install VLC manually.<br/>
You can find details instructions on how to do so <a href="https://docs.videolan.me/vlc-user/3.0/en/gettingstarted/setup/windows.html">here</a><br/><br/>

If you already ran the updater and it downloaded the installer, you can run it manually by opening a file explorer (Windows key + E, or just click the explorer icon) and enter <em>%TEMP%</em> as the location.<br/>
You'll see the installer there. It will be named «vlc-3.0.14-win32.exe» or «vlc-3.0.14-win64.exe» respectively depending on whether you're using a 32bit or 64bit version of Windows.<br/>
<br/>
<?php image("screenshots/3.0.12-update.jpg" , "3.0.12 update screen", "center-block img-responsive"); ?>
<br/>
<br/>

<h3>Post mortem explanation:</h3>
On May 10<sup>th</sup> 2021, the VideoLAN organization released VLC 3.0.13, and enabled auto updates.<br/>
This usually would be fairly straightforward, a prompt would appear informing you that an update is available, you click download and install, and that would be the end of it.<br/>
However and unfortunately, for this particular update, a few additional tedious (if not painful) steps will be necessary.<br/>
The issue was introduced in 3.0.12, but it didn't become obvious until we started rolling out 3.0.13.<br/>
While the issue was fixed for 3.0.14, we can't rely on that fix for people who've installed 3.0.12 already.<br/>

<br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/83d8e7efaa4f7dc23b07c47c59431e1f6df57da5">
The commit that introduced the bug</a><br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/d456994213b98933664bd6aee2e8f09d5dea5628">The commit that fixed the bug for future releases</a><br/>
