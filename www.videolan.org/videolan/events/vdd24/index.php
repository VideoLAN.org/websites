<?php
   $title = "13th Video Dev Days, November 2nd-3rd, 2024";
   $body_color = "orange";

   $new_design = true;
   $show_vdd_banner = false;
   $lang = "en";
   $menu = array( "videolan", "events" );

   $rev = time();

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/videolan/events/vdd24/style.css?$rev");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

  <div class="sponsor-box-2 d-xs-none d-md-block">
    <h4>Sponsors</h4>
    <a href="https://www.gemiso.com/" target="_blank">
      <?php image( 'events/vdd24/sponsors/geminiso.png' , 'Geminisoft', 'sponsors-logo'); ?>
    </a>
    <a href="https://www.kw.ac.kr/en/" target="_blank">
      <?php image( 'events/vdd24/sponsors/kwu.png' , 'KwangWoon University', 'sponsors-logo'); ?>
    </a>
    <a href="https://mux.com/" target="_blank">
      <?php image( 'events/vdd24/sponsors/mux.png' , 'Mux', 'sponsors-logo'); ?>
    </a>
    <a href="https://youtube.com/" target="_blank">
      <?php image( 'events/vdd24/sponsors/youtube_logo.svg' , 'YouTube', 'sponsors-logo'); ?>
    </a>
    <a href="https://videolabs.io/" target="_blank">
      <?php image( 'events/vdd24/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo-2'); ?>
    </a>
  </div>
  <header class="header-bg">
      <div class="container">
        <div class="row col-lg-10 col-lg-offset-1">
            <!--img class="img-fluid center-block" style="max-width:100%; height:auto; border-radius: 4%;" src="/images/events/vdd24/vdd24.png" alt="vdd logo"-->
	  <div class="col-sm-12 col-md-12 text-center align-middle" style="background: url('/images/events/vdd24/vdd24wide.png') no-repeat; background-size: contain; background-position:center; min-height:400px">
            <h1 id="videodevdays">Video Dev Days 2024</h1>
            <h3>The Open Multimedia Conference - Welcome to Cône-re-a !</h3>
            <h4>2nd - 3rd November, 2024, Seoul</h4>
          </div>
        </div>
      </div>
   </header>

<section id="overview">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">
			 About
          <span class="spacer-inline"></span>
        </h2>

<p><a href="/videolan/">The VideoLAN non-profit organization</a> is excited to invite you to the upcoming multimedia open-source event in Seoul !</p>

<p>In this edition, participants from the VideoLAN and open-source multimedia communities will gather in <strong>Seoul</strong> to discuss and collaborate on advancing the future of open-source multimedia.</p>

<p>This technical conference will focus on in-depth multimedia topics, including codecs and their implementations such as <a href="https://www.videolan.org/developers/x264.html">x264</a> and <a href="https://code.videolan.org/videolan/dav1d">dav1d</a>, as well as frameworks like FFmpeg, and projects like <strong>VideoLAN</strong>, <strong>VLC</strong>, <strong>Kodi</strong>, <strong>libplacebo</strong>, <strong>mpv</strong> and related projects.</p>

<p>This event is specifically geared towards the technical aspects of multimedia, emphasizing low-level components like codecs, their implementations, frameworks, playback libraries, and various innovative projects within the multimedia domain.</p>

		<div>
		  <div class="row">
          <div class="col-md-6">
            <div class="text-box" id="when-box">
              <h4 class="text-box-title">When ?</h4>
              <p class="text-box-content">2 - 3 November 2024</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box" id="where-box">
              <h4 class="text-box-title">Where ?</h4>
              <p class="text-box-content">Seoul, South Korea</p>
            </div>
          </div>
        </div>

		  <div class="row">
          <div class="col-md-6">
            <div class="text-box" id="who-come-box">
              <h4 class="text-box-title">For ?</h4>
              <p class="text-box-content">
        				<strong>Anyone</strong> who cares about open source multimedia technologies and development.
				  </p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box" id="partners-box">
              <h4 class="text-box-title">From ?</h4>
              <p class="text-box-content">VideoLAN. But you are very welcome to co-sponsor the event.</p>
            </div>
          </div>
        </div>
 		
		</div>

      </div>
    </div>
  </div>
</section>

<section id="registration">
  <div class="container">
    <h2 class="text-center uppercase">Registration</h2>
    <hr class="spacer-2">
    <div class="row text-center">
      <div class="col">
	Register for free at <a href="https://framaforms.org/vdd-2024-1729234952" target="_new">VDD 24 registration form</a>
      </div>
    </div>
  </div>
</section>

<section id="schedule">
  <div class="container">
    <div class="row">
      <div class="text-center">
        <h2 class="uppercase">Schedule</h2>
        <hr class="spacer-2">
      </div>
      <div class="col-lg-10 col-lg-offset-1">
      <!-- Nav tabs -->

      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#friday" aria-controls="friday" role="tab" data-toggle="tab">Friday 1</a></li>
        <li role="presentation" class="active"><a href="#saturday" aria-selected="true" aria-controls="saturday" role="tab" data-toggle="tab">Saturday 2</a></li>
        <li role="presentation"><a href="#sunday" aria-controls="sunday" role="tab" data-toggle="tab">Sunday 3</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade" id="friday">
            <div class="event">
              <h4 class="event-time">09:30</h4>
              <div class="event-description">
                <h3>Community Bonding Day</h3>
                <p>Meeting point is at Deoksugung Palace, ticket entrance, near the City Hall.
                </p>
              </div>
            </div>
            <div class="event">
              <h4 class="event-time">19:00</h4>
              <div class="event-description">
                <h3>Evening drinks</h3>
                <p>On <strong>Friday from 19h00</strong> people are welcome to come and share a few good drinks, with all attendees.</p>
                <p>Location: 행2PM8PM, 46-29 Jeo-dong 2(i)-ga, Jung District.</p>
              </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade active in" id="saturday">
            <div class="event event-breakfast">
              <h4 class="event-time">
                09:00
              </h4!-->
              <div class="event-description">
                <h3>Registration</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:30 - 10:00
              </h4>
              <p class="event-author">
                <span class="avatar avatar-jb"></span>Jean-Baptiste Kempf, VideoLAN
              </p>
              <div class="event-description">
                <h3>Opening remarks - Updates about community</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                10:00 - 10:20
              </h4>
              <p class="event-author">
                Kieran Kunhya, Open Broadcast Systems Ltd.
              </p>
              <div class="event-description">
                <h3>Ensuring the future of assembly language programming</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                10:20 - 10:40
              </h4>
              <p class="event-author">
                Anton Khirnov, FFmpeg
              </p>
              <div class="event-description">
                <h3>State of the ffmpeg CLI & MV-HEVC decoding</h3>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                10:40 - 11:00
              </h4>
              <div class="event-description">
                <h3>Break</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:00 - 11:25
              </h4>
              <p class="event-author">
                Henrik Gramner, Two Orioles
              </p>
              <div class="event-description">
                <h3>Checkasm</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:25 - 11:50
              </h4>
              <p class="event-author">
                Alaric Hamacher, Kwangwoon University
              </p>
              <div class="event-description">
                <h3>3D and Multiview -- FFMPEG Applications and Workflows</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:50 - 12:10
              </h4>
              <p class="event-author">
                Rémi-Denis Courmont, VideoLAN
              </p>
              <div class="event-description">
                <h3>Hijacking AI CPU extension for multimedia</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                12:10 - 12:30
              </h4>
              <p class="event-author">
                Wes Castro, Meta
              </p>
              <div class="event-description">
                <h3>Dynamic Filter Loading at Meta</h3>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                12:30 - 14:00
              </h4>
              <div class="event-description">
                <h3>Break</h3>
              </div>
            </div>

            <div class="event event-meetups">
              <h4 class="event-time">
                14:00 - 16:00
              </h4>
              <div class="event-description">
                <h3>Meetups session 1/2</h3>
                <div class="row">
                	<div class="col-md-6"><strong>Room 1</strong></div>
                	<div class="col-md-6"><strong>Room 2</strong></div>
                </div>
                <div class="row">
                	<div class="col-md-6">VLC Media Player</div>
                	<div class="col-md-6">FFmpeg</div>
                </div>
              </div>
            </div>

            <div class="event event-meetups">
              <h4 class="event-time">
                16:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Meetups session 2/2</h3>
                <div class="row">
                	<div class="col-md-6"><strong>Room 1</strong></div>
                	<div class="col-md-6"><strong>Room 2</strong></div>
                </div>
                <div class="row">
                	<div class="col-md-6"><p></p></div>
                	<div class="col-md-6"><p></p></div>
                </div>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                19:00 - 21:00
              </h4>
              <div class="event-description">
                <h3>Community Dinner</h3>
              </div>
            </div>

        </div>

        <div role="tabpanel" class="tab-pane fade" id="sunday">
            <div class="event event-talk">
              <h4 class="event-time">
                10:00 - 10:20
              </h4>
              <p class="event-author">
                Martin Storsjö, VideoLAN
              </p>
              <div class="event-description">
                <h3>LLVM-MingW updates</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                10:20 - 10:35
              </h4>
              <p class="event-author">
                Keith Herrington, KODI
              </p>
              <div class="event-description">
                <h3>KODI updates<h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                10:35 - 10:50
              </h4>
              <p class="event-author">
                Julien Zouein, Trinity College Dublin
              </p>
              <div class="event-description">
                <h3>Compression for Virtual Production</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                10:50 - 11:20
              </h4>
              <p class="event-author">
                Thomas Daede, Vimeo
              </p>
              <div class="event-description">
                <h3>IAMF</h3>
              </div>
            </div>


            <div class="event event-talk">
              <h4 class="event-time">
                11:00 - 12:15
              </h4>
              <p class="event-author">
                Nathan Egge, Google
              </p>
              <div class="event-description">
                <h3>RISC-V</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:15 - 11:25
              </h4>
              <p class="event-author">
                Dariusz Frankiewicz, Samsung
              </p>
              <div class="event-description">
                <h3>APV codec</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:25 - 11:35
              </h4>
              <p class="event-author">
                Martin Storsjö
              </p>
              <div class="event-description">
                <h3>Debuggging assembly functions</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:35 - 11:45
              </h4>
              <p class="event-author">
                Ramiro Polla 
              </p>
              <div class="event-description">
                <h3>FFGlitch</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:45 - 11:50
              </h4>
              <p class="event-author">
                Dmitriy	Kovalenko
              </p>
              <div class="event-description">
                <h3>Time to generate videos</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                11:50 - 12:00
              </h4>
              <p class="event-author">
                 Ronald Bultje, Two Orioles
              </p>
              <div class="event-description">
                <h3>dav1d update</h3>
              </div>
            </div>



            <div class="event event-lunch">
              <h4 class="event-time">
                12:15 - 13:30
              </h4>
              <div class="event-description">
                <h3>Lunch Break</h3>
              </div>
            </div>

            <div class="event event-meetups">
              <p class="event-author">
                Niklas Haas,  Marvin Scholz, Jan Ekström, James Kuo-Ping Lo, etc...
              </p>
              <h4 class="event-time">
                13:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Unconferences</h3>
                <div class="row">
                	<div class="col-md-4"><strong>Room 1</strong></div>
                	<div class="col-md-4"><strong>Room 2</strong></div>
                	<div class="col-md-4"><strong>Room 3</strong></div>
                </div>
                <div class="row">
                	<div class="col-md-4"><p>13:30 VideoLAN Asso Meeting</p></div>
                	<div class="col-md-4"><p></p></div>
                	<div class="col-md-4"><p></p></div>
                </div>
              </div>
            </div>

        </div>
        
      </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Sponsors</h2>
    <hr class="spacer-2">

    <section class="text-center">
     <div class="row col-xs-10 col-xs-offset-1">
	     <div class="col col-md-6 col-sm-12">
		<a href="https://www.gemiso.com/" target="_blank">
                 <?php image( 'events/vdd24/sponsors/geminiso.png' , 'Geminisoft', 'sponsors-logo'); ?>
    		</a>
            </div>
	     <div class="col col-md-6 col-sm-12">
		<a href="https://www.kw.ac.kr/en/" target="_blank">
                 <?php image( 'events/vdd24/sponsors/kwu.png' , 'KwangWoon University', 'sponsors-logo'); ?>
    		</a>
            </div>

            <div class="clearfix"></div>

	     <div class="col col-md-6 col-sm-12">
	       <a href="https://mux.com/" target="_blank">
                <?php image( 'events/vdd24/sponsors/mux.png' , 'Mux', 'sponsors-logo'); ?>
              </a>
            </div>
	     <div class="col col-md-6 col-sm-12">
              <a href="https://youtube.com/" target="_blank">
                <?php image( 'events/vdd24/sponsors/youtube_logo.svg' , 'YouTube', 'sponsors-logo'); ?>
              </a>
            </div>

          <div class="clearfix"></div>

	      <div class="col col-md-6 col-sm-12">
        	<a href="https://videolabs.io/" target="_blank">
	          <?php image( 'events/vdd24/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo-2'); ?>
	        </a>
	      </div>
     </div>
    </section>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Access</h2>
    <hr class="spacer-2">
    <div class="row">
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Conference</h3>
          <h4>Kwangwoon University</h4>
          <p>Saebit Building (새빛관), 20, Gwangun-ro 6-gil, Nowon-gu, Seoul</p>
          <p>Kwangwoon Univ. Station (Subway Line No.1), Seokgye Station (Subway Line No. 6), Blue Buses (261 and 163), Green Buses (1017, 1137, 1140 and Seongbuk12)</p>
          <iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=127.05826073884965%2C37.61846365727354%2C127.06350713968278%2C37.62110018816536&amp;layer=mapnik&amp;marker=37.61978193440671%2C127.0608839392662" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=37.619782&amp;mlon=127.060884#map=19/37.619782/127.060884">Open map in full</a></small>

      </div>
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Hotels</h3>
	<p>If you have been sponsored, your accomodation will be one of the following: </p>
        <h4>Hotel Skypark Central Myeongdong</h4>
        <p>16 Myeongdong 9-gil, Jung District
        <iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=126.98375433683397%2C37.56384032097282%2C126.98600739240649%2C37.565334967514836&amp;layer=mapnik" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/37.564588/126.984881">Open map in full</a></small>	
        </p>
        <div class="clearfix"></div>
        <h4>Travelodge Myeongdong Euljiroi</h4>
        <p>61 Supyo-ro, Jung District
        <iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=126.98704272508623%2C37.565853728294165%2C126.99023455381396%2C37.567348334439394&amp;layer=mapnik" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/37.566601/126.988974">Open map in full</a></small>	
        </p>
        <div class="clearfix"></div>
	<h4>Mangrove Hotel Sinseol</h4>
        <p>22 Wangsan-ro, Dongdaemun District
        <iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=127.02493965625764%2C37.57539488057301%2C127.02719271183015%2C37.57688929526045&amp;layer=mapnik" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/37.576142/127.026066">Open map in full</a></small>	
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Beer Event</h3>
        <p>행2PM8PM, 46-29 Jeo-dong 2(i)-ga, Jung District
        <iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=126.98319911956789%2C37.56146115601512%2C126.99221134185792%2C37.56743975318769&amp;layer=mapnik" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=17/37.564451/126.987705">Open map in full</a></small>	
        </p>
      </div>
      <div class="col-md-6">
      	<h3 class="text-center uppercase">Community Dinner</h3>
        <p>Hangaram (한가람), 25 Namdaemun-ro 5-gil, Jung District
        <iframe width="75%" height="200" src="https://www.openstreetmap.org/export/embed.html?bbox=126.97572112083436%2C37.561040169853904%2C126.98022723197938%2C37.564029545308195&amp;layer=mapnik" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/37.562535/126.977974">Open map in full</a></small>	
        </p>
      </div>
    </div>
  </div>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="uppercase">Code of Conduct </h2>
        <p>This community activity is running under the <a href="https://wiki.videolan.org/CoC/">VideoLAN Code of Conduct</a>. We expect all attendees to respect our <a href="https://wiki.videolan.org/VideoLAN_Values/">Shared Values</a>.</p>
      </div>
      <div class="col-md-6">
        <h2 class="uppercase">Contact </h2>
        <p>The VideoLAN Dev Days are organized by the board members of the VideoLAN non-profit organization, Jean-Baptiste Kempf, Denis Charmet, Felix Paul Khüne and Konstantin Pavlov. You can reach us at <span style="color: #39b549">board@videolan.org</span>.</p>
      </div>
      <div class="col-md-6">
        <h2 class="uppercase">Local Organizer </h2>
        <p>Sungjoon Moon, Alaric Hamacher</p>
      </div>
    </div>
  </div>
</section>

<?php footer('$Id: index.php 5400 2009-07-19 15:37:21Z jb $'); ?>
