# Khmer translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Sokhem Khoem <sokhem@open.org.kh>, 2012-2013
# Sok Sophea <sksophea@gmail.com>, 2013-2014
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2020-12-13 15:08+0100\n"
"Last-Translator: Pokno Royal​ (ប៉ុកណូ រ៉ូយ៉ាល់) <royalpokno68@gmail.com>, 2020\n"
"Language-Team: Khmer (http://www.transifex.com/yaron/vlc-trans/language/"
"km/)\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: include/header.php:289
msgid "a project and a"
msgstr "ជាគម្រោង និងជា"

#: include/header.php:289
msgid "non-profit organization"
msgstr "អង្គការ​មិន​រក​ប្រាក់​ចំណេញ"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "ដៃគូ"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "ក្រុម និងអង្កការ"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "សេវាកម្មប្រឹក្សាយោបល់ និងដៃគូ"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "ព្រឹត្តិការណ៍"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "ស្រប​ច្បាប់"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "មជ្ឈមណ្ឌល​ព័ត៌មាន"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "ទាក់ទង​យើង​ខ្ញុំ"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "ទាញយក"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "លក្ខណៈ"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "ប្តូរតាមបំណង"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "យក​ធាតុ​ផ្សេងៗ"

#: include/menus.php:51
msgid "Projects"
msgstr "គម្រោង"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "គម្រោង​ទាំងអស់"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "ចូលរួម"

#: include/menus.php:77
msgid "Getting started"
msgstr "ចាប់ផ្តើមទទួលយក"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "បរិច្ចាគ"

#: include/menus.php:79
msgid "Report a bug"
msgstr "រាយការណ៍​កំហុស"

#: include/menus.php:83
msgid "Support"
msgstr "គាំទ្រ"

#: include/footer.php:33
msgid "Skins"
msgstr "រូបរាង"

#: include/footer.php:34
msgid "Extensions"
msgstr "ផ្នែក​បន្ថែម"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "រូបថត​អេក្រង់"

#: include/footer.php:61
msgid "Community"
msgstr "សហគមន៍"

#: include/footer.php:64
msgid "Forums"
msgstr "វេទិកា"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "បញ្ជី​សំបុត្រ​រួម"

#: include/footer.php:66
msgid "FAQ"
msgstr "សំណួរ​ដែល​សួរ​រឿយៗ"

#: include/footer.php:67
msgid "Donate money"
msgstr "​ឧបត្ថម្ភ​ជា​​ថវិកា"

#: include/footer.php:68
msgid "Donate time"
msgstr "​ឧបត្ថម្ភ​ជា​ពេលវេលា"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "គម្រោង និង​អង្គការ"

#: include/footer.php:76
msgid "Team"
msgstr "ក្រុម"

#: include/footer.php:80
msgid "Mirrors"
msgstr "កញ្ចក់"

#: include/footer.php:83
msgid "Security center"
msgstr "មជ្ឈមណ្ឌល​សុវត្ថិភាព"

#: include/footer.php:84
msgid "Get Involved"
msgstr "ចូល​រួម"

#: include/footer.php:85
msgid "News"
msgstr "ព័ត៌មាន​"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "ទាញ​យក VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "ប្រព័ន្ធ​ផ្សេង"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "ការទាញយករហូតមកដល់ពេលនេះ"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC គឺជា​កម្មវិធី​ចាក់​ពហុ​មេឌៀ​​​កូដ​ចំហ​​ឥតគិតថ្លៃ​ដែល​ដំណើរការ​លើ​ពហុ​ប្រព័ន្ធ​ប្រតិបត្តិការ​ ហើយ​អាច​ចាក់​ឯកសារ​ពហុ​"
"មេឌៀ​បាន​ច្រើន​មាន​ដូចជា​ឌីវីឌី ស៊ីឌី​អូឌីយ៉ូ វីស៊ីឌី និង​ពិធីការ​ស្ទ្រីម​ផ្សេង​ទៀត។"

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC គឺឥតគិតថ្លៃ និងជាកម្មវិធីចាក់ពហុមេឌៀដែលមានទម្រង់ឆ្លងប្រព័ន្ធបើកចំហរ "
"និងមានតួនាទីចាក់រាល់ឯកសារពហុមេឌៀ និងពិធីការ​ស្ទ្រីមផ្សេងៗពីគ្នា។"

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC ៖ តំបន់​បណ្ដាញ​ផ្លូវការ - កម្មវិធី​ចាក់​ពហុ​មេឌៀ​ដោយ​គិត​ថ្លៃ​សម្រាប់​គ្រប់​ប្រព័ន្ធ​ប្រតិបត្តិការ!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "គម្រោង​ផ្សេង​ទៀត​ពី VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "សម្រាប់​អ្នក​រាល់​គ្នា"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC ជា​កម្មវិធី​ចាក់​មេឌៀ​ដ៏​មាន​អានុភាព ដែល​ចាក់​កូឌិក​មេឌៀ​ភាគ​ច្រើន និង​​ទ្រង់ទ្រាយ​វីដេអូ​ជា​ច្រើន​ ។"

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr "កម្មវិធី​បង្កើត​ភាពយន្ត VideoLAN គឺ​ជា​កម្មវិធី​កែសម្រួល​សម្រាប់ការ​បង្កើត​វីដេអូ ។"

#: index.php:62
msgid "For Professionals"
msgstr "សម្រាប់​អ្នក​ជំនាញ"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr "DVBlast គឺជា​ MPEG-2/TS demux សាមញ្ញ​ និង​ដ៏​មាន​អានុភាព ព្រម​ទាំង​ជា​កម្មវិធី​ស្ទ្រីម ។"

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat គឺ​ជា​បណ្ដុំ​កម្មវិធី​ ដែល​បាន​រចនា​ ដើម្បី​គណនា​ស្ទ្រីម​ម៉ាល់ទីខាស និង TS ​យ៉ាង​ងាយស្រួល និង​មាន​"
"ប្រសិទ្ធភាព ។"

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr "x264 គឺ​ជា​កម្មវិធី​ឥត​គិត​ថ្លៃ​សម្រាប់​អ៊ីនកូដ​ស្ទ្រីម​វីដេអូ​ទៅ​ជា​​ទ្រង់ទ្រាយ H.264/MPEG-4 AVC ។"

#: index.php:104
msgid "For Developers"
msgstr "សម្រាប់​អ្នក​អភិវឌ្ឍន៍"

#: index.php:140
msgid "View All Projects"
msgstr "មើល​គម្រោង​ទាំងអស់"

#: index.php:144
msgid "Help us out!"
msgstr "ជួយ​យើង !"

#: index.php:148
msgid "donate"
msgstr "ឧបត្ថម្ភ"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN គឺ​ជា​អង្គការ​មិន​រក​ប្រាក់​ចំណេញ ។"

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
" តម្លៃ​របស់​យើង​ទាំងអស់​បំពេញ​តាម​ការ​ឧបត្ថម្ភ ដែល​យើង​បាន​ទទួល​ពី​អ្នកប្រើប្រាស់​របស់​យើង ។ ប្រសិន​បើ​អ្នក​"
"រីករាយ​ប្រើ​ផលិតផល VideoLAN សូម​ជួយ​ឧបត្ថម្ភ​យើង​​ខ្ញុំ ។"

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "ស្វែងយល់​បន្ថែម"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN គឺ​ជា​កម្មវិធី​កូដ​ចំហ"

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"នេះ​មាន​ន័យ​ថា ប្រសិន​បើ​​អ្នក​មាន​ជំនាញ និង​ចង់​បង្កើន​ផលិតផល​មួយ​ក្នុង​ចំណោម​ផលិតផល​របស់​យើង សូម​ស្វាគមន៍​ការ​"
"ចូលរួម​របស់​អ្នក"

#: index.php:187
msgid "Spread the Word"
msgstr "ចែកចាយ​ទូទាំង​ពិភពលោក"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"យើង​មាន​អាម្មណ៍​ថា VideoLAN មាន​កម្មវិធី​សម្រាប់​ចាក់​វីដេអូ​ដ៏​ល្អ​បំផុត ដោយ​មិនគិត​ថ្លៃ ។ ប្រសិនបើ​អ្នក​"
"យល់ព្រម សូម​ជួយ​ចែកចាយ​កម្មវិធី​របស់​យើង​ទូទាំង​ពិភព​លោក ។"

#: index.php:215
msgid "News &amp; Updates"
msgstr "ព័ត៌មាន &amp; បច្ចុប្បន្នភាព"

#: index.php:218
msgid "More News"
msgstr "ពត៌មានបន្ថែម"

#: index.php:222
msgid "Development Blogs"
msgstr "កំណត់​ហេតុ​បណ្ដាញ​អភិវឌ្ឍន៍"

#: index.php:251
msgid "Social media"
msgstr "មេឌៀ​សង្គម"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr "ទាញយកជាផ្លូវការនូវកម្មវិធីចាក់មេឌៀ VLC ដែលជាកម្មវិធីចាក់មេឌៀ ប្រភព​កូដ​ចំហដ៏ល្អបំផុត"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "យក​ VLC សម្រាប់"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "សាមញ្ញ លឿន និងថាមពលខ្លាំង"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "លេងអ្វីៗគ្រប់យ៉ាង"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "ឯកសារ ឌីសថាស​ កាមេរ៉ាវែប ឧបករណ៍ និងការស្ទ្រីម។"

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "ចាក់លេងកូឌិកភាគច្រើនដោយមិនចាំបាច់ប្រើកញ្ចប់កូដិកទេ"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "ដំណើរការលើគ្រប់ទម្រង់ប្រព័ន្ធ"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "ឥតគិតថ្លៃទាំងស្រុង"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "គ្មាន​កម្មវិធី​រំខាន គ្មានការ​ផ្សាយ​ពាណិជ្ជកម្ម និង​គ្មាន​កា​រតាមដានអ្នកប្រើប្រាស់។"

#: vlc/index.php:47
msgid "learn more"
msgstr "ស្វែងយល់បន្ថែម"

#: vlc/index.php:66
msgid "Add"
msgstr "បន្ថែម"

#: vlc/index.php:66
msgid "skins"
msgstr "ស្បែក"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "បង្កើតស្បែកជាមួយ"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "កម្មវិធីកែស្បែក VLC"

#: vlc/index.php:72
msgid "Install"
msgstr "ដំឡើង"

#: vlc/index.php:72
msgid "extensions"
msgstr "ផ្នែកបន្ថែម"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "មើល​រូបថត​អេក្រង់​ទាំងអស់"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "ទាញយក​កម្មវិធី​ចាក់​មេឌៀ VLC ជា​ផ្លូវការ"

#: vlc/index.php:146
msgid "Sources"
msgstr "ប្រភព"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "អ្នក​ក៏​អាច​យក​ដោយ​ផ្ទាល់នូវ"

#: vlc/index.php:148
msgid "source code"
msgstr "កូដ​ប្រភព"

#~ msgid "A project and a"
#~ msgstr "ជា​គម្រោង និង​​ជា"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr "មាន​អ្នក​ស្ម័គ្រ​ចិត្ត ដែល​កំពុង​អភិវឌ្ឍ និង​ផ្សព្វផ្សាយ​កម្មវិធី​ចាក់​មេឌៀ​កូដ​ចំហ ។"

#~ msgid "why?"
#~ msgstr "ហេតុ​អ្វី ?"

#~ msgid "Home"
#~ msgstr "ដើម"

#~ msgid "Support center"
#~ msgstr "មជ្ឈមណ្ឌល​គាំទ្រ"

#~ msgid "Dev' Zone"
#~ msgstr "តំបន់​អភិវឌ្ឍន៍"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "កម្មវិធី​ចាក់​មេឌៀ​សាមញ្ញ លឿន និង​​មាន​អនុភាព ។"

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "ចាក់​អ្វីៗ​គ្រប់យ៉ាង ៖ ឯកសារ ថាស វិបខេម ឧបករណ៍ និង​ស្ទ្រីម​ ។"

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "ចាក់​កូឌិក​ភាគ​ច្រើន​ដោយ​មិន​ចាំបាច់​ត្រូវការ​កញ្ចប់​កូឌិក ៖"

#~ msgid "Runs on all platforms:"
#~ msgstr "ដំណើរការ​គ្រប់​ប្រព័ន្ធ​ប្រតិបត្តិការ ៖"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "ពិតជា​ឥតគិតថ្លៃ គ្មាន​កម្មវិធី​រំខាន គ្មាន​ការ​ផ្សាយ​ពាណិជ្ជកម្ម និង​គ្មាន​ការ​តាមដាន​អ្នកប្រើ។"

#~ msgid "Can do media conversion and streaming."
#~ msgstr "អាច​បម្លែង​មេឌៀ​ និង​ស្ទ្រីម"

#~ msgid "Discover all features"
#~ msgstr "មើល​​គ្រប់លក្ខណៈ"

#~ msgid "DONATE"
#~ msgstr "ឧបត្ថម្ភ"

#~ msgid "Other Systems and Versions"
#~ msgstr "កំណែ និង​ប្រព័ន្ធ​ប្រតិបត្តិការ​ផ្សេង"

#~ msgid "Other OS"
#~ msgstr "ប្រព័ន្ធ​ប្រតបត្តិកា​រផ្សេង"
