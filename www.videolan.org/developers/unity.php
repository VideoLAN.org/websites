<?php
    $title = "VLC for Unity";
    $lang = "en";
    $new_design = true;
    $body_color = "red";
    require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<div class="container">
    <h1 class="bigtitle">VLC for Unity</h1>

    <p><code>vlc-unity</code> is an integration of the LibVLC engine with the Unity 3D game engine. This project allows to embed <a href="https://www.videolan.org/vlc/libvlc.html">LibVLC</a> inside a Unity-based game.</p>

    <p>Since Unity apps and games are built using C#, <a href="https://code.videolan.org/videolan/LibVLCSharp">LibVLCSharp</a> is used for the scripting part when interacting with LibVLC in Unity. The rest of the plugin code, transparent to Unity users, is written in C++ for maximum performance and ease of use of the various graphics APIs (<b>OpenGL, Direct3D</b>).</p>

    <img style="width:100%" src="//images.videolan.org/images/vlc-unity.png" alt="VLC for Unity"/>

    <div class="row">
        <div class="column col-xs-12 col-md-6">
        <h2>Supported platforms</h2>
        <br/>
        <ul>
            <li>
                <strong>Windows Classic</strong>
                <ul>
                    <li>- Minimum OS version: Windows 7</li>
                    <li>- ABI supported: x64</li>
                    <li>- Graphics API: Direct3D 11</li>
                </ul>
            </li>
            <br/>
            <li>
                <strong>Windows Modern (UWP)</strong>
                <ul>
                    <li>- Minimum OS version: Windows 10</li>
                    <li>- ABI supported: x64, ARM64</li>
                    <li>- Graphics API: Direct3D 11</li>
                </ul>
            </li>
            <br/>
            <li>
                <strong>Android</strong>
                <ul>
                    <li>- Minimum OS version: Android 4.2 (API 17)</li>
                    <li>- ABI supported: armeabi-v7a, arm64-v8a, x86, x86_64</li>
                    <li>- Graphics API: OpenGL ES 2/3</li>
                </ul>
            </li>
            <br/>
            <li>
                <strong>iOS</strong>
                <ul>
                    <li>- Minimum OS version: iOS 9</li>
                    <li>- ABI supported: ARM64. Simulator support will come later.</li>
                    <li>- Graphics API: Metal</li>
                </ul>
            </li>
            <br/>
            <li>
                <strong>macOS</strong>
                <ul>
                    <li>- Minimum OS version: macOS 10.11</li>
                    <li>- ABI supported: Intel x64, Apple Silicon ARM64</li>
                    <li>- Graphics API: Metal</li>
                </ul>
            </li>
        </ul>

        </div>
        <div class="column col-xs-12 col-md-6">
            <h2>Sample scenes</h2>

            Several Unity sample scenes are available to help you get started quickly!
            <p>
                <ul>
                    <li>- A minimal playback example with buttons</li>
                    <li>- 360 playback with keyboard navigation built-in</li>
                    <li>- A video with subtitles showcasing support</li>
                    <li>- The VLCPlayerExample provides a great base with more controls</li>
                    <li>- 3D scene you can move around in with a movie screen and chairs in a cinema room</li>
                </ul>
            </p>
            <h2>Support</h2>
            <p>User support and development is handled in the <a href="https://code.videolan.org/videolan/vlc-unity">GitLab repository</a>.</p>
            <h2>Development</h2>
            <p><code>vlc-unity</code> is available through <a href="https://wiki.videolan.org/git">git</a> at:</p>
            <pre><code>git clone https://code.videolan.org/videolan/vlc-unity</code></pre>

            <p>The source code can be browsed online at <a href="https://code.videolan.org/videolan/vlc-unity">code.videolan.org</a>.</p>
            <p>Contributions are welcome as merge requests in our <a href="https://code.videolan.org/videolan/vlc-unity/-/merge_requests">gitlab repository</a>.</p>
        </div>
    </div>    
    <div class="row">
        <div class="column col-xs-12 col-md-6">
            <img style="width: 100%" src="//images.videolan.org/images/unity-3d-scene.png" alt="3d scene in Unity"/>
        </div>
        <div class="column col-xs-12 col-md-6">
            <img style="width: 100%" src="//images.videolan.org/images/unity-media-element.png" alt="media element Unity"/>
        </div>
    </div>
</div>
<?php footer(); ?>
